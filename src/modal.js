import React from 'react';
import PropTypes from 'prop-types';
import { Button, Modal, Item } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import { withInfiniteScroll } from './scroll';
import pullData from './api';

const ModalContent = withInfiniteScroll(pullData)(Modal.Content);
const ModalWithScroller = props => (
  <Modal trigger={<Button>Show Gallery</Button>}>
    <Modal.Header>Gallery</Modal.Header>
    <ModalContent image {...props}>
      <Item.Group divided>
        {props.data.map((d, index) => (
          <Item key={d.id}>
            <Item.Image src={d.imageSrc} />
            <Item.Content verticalAlign="middle">{d.content} - position {index + 1}</Item.Content>
          </Item>
        ))}
        {props.moreData &&
        (
          <Item>
            <Item.Content style={{ textAlign: 'center' }} verticalAlign="middle">More pics...</Item.Content>
          </Item>
        )
        }
      </Item.Group>
    </ModalContent>
  </Modal>
);

ModalWithScroller.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object.isRequired).isRequired,
  moreData: PropTypes.bool.isRequired,
};

export default ModalWithScroller;
