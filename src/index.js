import React, { Component } from 'react';
import { render } from 'react-dom';
import Modal from './modal';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { data: [], moreData: true };
    this.updateData = this.updateData.bind(this);
  }

  updateData({ data, moreData }) {
    this.setState(prevState => ({ data: [...prevState.data, ...data], moreData }));
  }

  render() {
    return (
      <Modal update={this.updateData} {...this.state} />
    );
  }
}

render(<App />, document.getElementById('root'));
