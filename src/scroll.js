import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './style.scss';

const withInfiniteScroll = handler =>
  function getData(WrappedComponent) {
    class WrappedScrollComponent extends Component {
      constructor(props) {
        super(props);
        this.onScroll = this.onScroll.bind(this);
        this.pullData = this.pullData.bind(this);
        this.lastScrollPosition = 0;
      }

      componentDidMount() {
        this.pullData(this.props.data.length);
      }

      componentWillReceiveProps() {
        setTimeout(() => {
          this.scroller.scrollTop = this.lastScrollPosition;
        }, 500);
      }

      onScroll() {
        const maxScrollTop = this.scroller.scrollHeight - this.scroller.clientHeight;
        if (this.scroller.scrollTop >= maxScrollTop && this.props.moreData) {
          this.lastScrollPosition = this.scroller.scrollTop;
          this.pullData(this.props.data.length);
        }
      }


      pullData(size) {
        handler(size).then((data) => {
          this.props.update({ data, moreData: true });
        }).catch(() => this.props.update({ data: [], moreData: false }));
      }

      render() {
        return (
          <div
            className="scroll-container"
            ref={(scroller) => {
              this.scroller = scroller;
            }}
            onScroll={this.onScroll}
          >
            <WrappedComponent>
              {this.props.children}
            </WrappedComponent>
          </div>
        );
      }
    }

    WrappedScrollComponent.propTypes = {
      children: PropTypes.element.isRequired,
      update: PropTypes.func.isRequired,
      moreData: PropTypes.bool.isRequired,
      data: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        content: PropTypes.string.isRequired,
        imageSrc: PropTypes.string.isRequired,
      }).isRequired).isRequired,
    };

    return WrappedScrollComponent;
  };

export { withInfiniteScroll };
