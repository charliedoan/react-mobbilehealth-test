import uuid from 'uuid/v4';
import imageSrc from './assets/images/vietnam.jpg';

const createFakeData = (size) => {
  const limitedCapacity = 105;
  if (size < limitedCapacity) {
    const available = limitedCapacity - size;
    const responseSize = (available > 25) ? 25 : available;
    return Array(responseSize).fill(1).map(() => ({
      id: uuid(), content: new Date().toLocaleString(), imageSrc,
    }));
  }
  return null;
};

const fakeApi = size => new Promise((resolve, reject) => {
  const data = createFakeData(size);
  setTimeout(() => {
    if (data) resolve(data);
    else reject(new Error('no more data'));
  }, 1000);
});


const pullData = size => new Promise((resolve, reject) => {
  fakeApi(size).then(data => resolve(data)).catch(error => reject(error));
});

export default pullData;
