const merge = require('webpack-merge');
const { HotModuleReplacementPlugin, DefinePlugin } = require('webpack');
const commonConfig = require('./webpack.common');

const devConfig = {
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist',
    hot: true,
    port: 9000,
  },
  plugins: [
    new HotModuleReplacementPlugin(),
    new DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development'),
    }),
  ],
};

module.exports = merge(commonConfig, devConfig);
