module.exports = {
    "extends": "airbnb",
    "globals": {
        "document": false,
    },
    "plugins": [
        "react",
        "import"
    ],
    "rules": {
        "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
    }
};