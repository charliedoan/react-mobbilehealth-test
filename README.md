# install
```bash
  npm install

  // start dev
  npm run start

  // build production
  npm run build

  // run eslint
  npm run lint 
```